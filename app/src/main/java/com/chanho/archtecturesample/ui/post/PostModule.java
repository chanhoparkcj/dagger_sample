package com.chanho.archtecturesample.ui.post;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chanho.archtecturesample.databinding.FragmentPostBinding;
import com.chanho.archtecturesample.di.ApplicationContext;
import com.chanho.archtecturesample.di.FragmentScope;

import dagger.Module;
import dagger.Provides;

@Module
public class PostModule {
    //데이터 바인딩 클래스 제공
    @Provides
    @FragmentScope
    FragmentPostBinding provideBinding(@ApplicationContext Context context){
        return FragmentPostBinding.inflate(LayoutInflater.from(context),null,false);
    }

    //Navigation 컴포넌트에서 목적지간 이동을 담당하는 NavCOntroller
//    @Provides
//    @FragmentScope
//    NavController provideNavController(PostFragment fragment){
//        return NavHostFragment.findNavController(fragment);
//    }

    //RecyclerView 용 레이아웃 매니져
    @Provides
    @FragmentScope
    LinearLayoutManager provideLinearLayoutManager(@ApplicationContext Context context){
        return new LinearLayoutManager(context){
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        };
    }
}
