package com.chanho.archtecturesample.ui.post;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chanho.archtecturesample.BR;
import com.chanho.archtecturesample.R;
import com.chanho.archtecturesample.util.ViewBindingHolder;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * PostAdapter 게시글 목록을 위한
 */
public class PostAdapter extends RecyclerView.Adapter<ViewBindingHolder> {

    private final List<PostItem> items = new ArrayList<>();

    // 생성자 인젝션
    @Inject
    public PostAdapter(){

    }
    //레이아웃 종류

    @Override
    public int getItemViewType(int position) {
        return R.layout.view_post;
    }

    //뷰 홀더 생성
    @NonNull
    @Override
    public ViewBindingHolder<?> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewBindingHolder(parent.getContext(),viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewBindingHolder holder, int position) {
        holder.getBinding().setVariable(BR.item, items.get(position));
        holder.getBinding().executePendingBindings();
    }

    //외부로부터 글 목록을 받아 ui 를 갱신한다
    public void setItems(List<PostItem> items){
        this.items.clear();
        this.items.addAll(items);
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}
