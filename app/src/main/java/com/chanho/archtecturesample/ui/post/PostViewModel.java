package com.chanho.archtecturesample.ui.post;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.chanho.archtecturesample.data.PostService;
import com.chanho.archtecturesample.util.SingleLiveEvent;

import java.util.List;
import java.util.Timer;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import timber.log.Timber;

public class PostViewModel extends AndroidViewModel  {

    @NonNull
    private final PostService postService;
    @NonNull
    private final SingleLiveEvent<Throwable> errorEvent;

    private final SingleLiveEvent<PostItem> postClickEvent = new SingleLiveEvent<>();

    //RecyclerView 에 표현할 아이템들을 LiveData로 관리
    private final MutableLiveData<List<PostItem>> livePosts = new MutableLiveData<>();

    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    private final MutableLiveData<Boolean> loading = new MutableLiveData<>(true);

    @Inject
    public PostViewModel(@NonNull Application application,
                         PostService postService,
                         @Named("errorEvent") SingleLiveEvent<Throwable> errorEvent) {

        super(application);
        Timber.d("PostViewModel created");
        // 그래프로부터 생성자 주입
        this.postService = postService;
        this.errorEvent = errorEvent;
    }

    //게시글 불러오기
    public void loadPost(){
        compositeDisposable.add(postService.getPosts().flatMapObservable(Observable::fromIterable)
        .map(post -> new PostItem(post))
        .toList()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .doOnSuccess(item -> loading.postValue(false))
        .subscribe(livePosts::setValue,errorEvent::setValue));
    }


    public MutableLiveData<List<PostItem>> getLivePosts() {
        return livePosts;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        Timber.d("onCleared");
        compositeDisposable.dispose();
    }

    public MutableLiveData<Boolean> getLoading() {
        return loading;
    }

    @Override
    public void onPostClick(PostItem postItem) {
        postClickEvent.setValue(postItem);
    }

    public SingleLiveEvent<PostItem> getPostClickEvent(){
        return postClickEvent;
    }
}
