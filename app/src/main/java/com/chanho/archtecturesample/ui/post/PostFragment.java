package com.chanho.archtecturesample.ui.post;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chanho.archtecturesample.R;
import com.chanho.archtecturesample.databinding.FragmentPostBinding;
import com.chanho.archtecturesample.di.AppViewModelFactory;

import javax.inject.Inject;

import dagger.Lazy;
import dagger.android.support.DaggerFragment;

public class PostFragment extends DaggerFragment {

    /**
     * 오브젝트 그래프로부터 멤버 인젝션
     */
    @Inject
    FragmentPostBinding binding;

    @Inject
    AppViewModelFactory viewModelFactory;

    @Inject
    PostAdapter adapter;

    @Inject
    LinearLayoutManager layoutManager;

    PostViewModel viewModel;

//    @Inject
//    Lazy<NavController> navController;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // viewmodel 객체 요청
        viewModel = new ViewModelProvider(this,viewModelFactory).
                get(PostViewModel.class);
        if(savedInstanceState==null){
            viewModel.loadPost();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        return inflater.inflate(R.layout.fragment_post, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.setLifecycleOwner(getViewLifecycleOwner());
        binding.recyclerView.setAdapter(adapter);
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.setViewModel(viewModel);
        viewModel.getLivePosts() .observe(getViewLifecycleOwner(),list ->
                adapter.setItems(list));

//        게시글이 클릭 되었을때 게시글 상세 화면 목적지로 이동
//        viewModel.getPostClickEvent()
//                .observe(getViewLifecycleOwner(),postItem -> navController.get().navigate());


    }
}