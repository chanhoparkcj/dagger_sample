package com.chanho.archtecturesample.ui;

import android.content.Context;

import androidx.databinding.DataBindingUtil;

import com.chanho.archtecturesample.R;
import com.chanho.archtecturesample.databinding.ActivityMainBinding;
import com.chanho.archtecturesample.di.ActivityContext;
import com.chanho.archtecturesample.di.ActivityScope;
import com.chanho.archtecturesample.di.FragmentScope;
import com.chanho.archtecturesample.ui.MainActivity;
import com.chanho.archtecturesample.ui.post.PostFragment;
import com.chanho.archtecturesample.ui.post.PostModule;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainModule {

    @Provides
    @ActivityScope
    static ActivityMainBinding provideBinding(MainActivity activity){
        return DataBindingUtil.setContentView(activity, R.layout.activity_main);
    }

    @Provides
    @ActivityContext
    static Context provideContext(MainActivity activity){
        return activity;
    }

    /**
     * 서브컴포넌트 정의
     */
    @FragmentScope
    @ContributesAndroidInjector(modules = PostModule.class)
    abstract PostFragment getPostFragment();
}

